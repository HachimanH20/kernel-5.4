/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * Copyright (c) 2020-2021, The Linux Foundation. All rights reserved.
 */

#define CONFIG_DRM_MSM_HYP 1
#define CONFIG_DRM_MSM_HYP_WFD 1
#define CONFIG_SYNC_FILE 1
#define CONFIG_DRM_MSM_LEASE 1
