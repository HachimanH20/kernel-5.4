// SPDX-License-Identifier: GPL-2.0-only
/*
 * Copyright (c) 2021, The Linux Foundation. All rights reserved.
 */

#define CREATE_TRACE_POINTS
#include "msm_hyp_trace.h"
